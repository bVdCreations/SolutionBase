from datetime import datetime, date, timedelta


def get_week_number(date):
    '''returns the weeknumber of the given date'''
    return date.isocalendar()[1]


def get_iso_year(date):
    '''returns the isoyear of the given date'''
    return date.isocalendar()[0]


def this_iso_year():
    '''returns the isoyear of the today'''
    return get_iso_year(date.today())


def this_week():
    '''returns the week number of today'''
    return get_week_number(date.today())


def week_range(date):
    """Find the first/last day of the week for the given day.
    Assuming weeks start on Monday and end on Sunday.

    Returns a tuple of ``(start_date, end_date)``.

    """
    # source: https: // bradmontgomery.net/blog/calculate-week-range-date/

    # isocalendar calculates the year, week of the year, and day of the week.
    # dow is Mon = 1, Sat = 6, Sun = 7
    year, week, dow = date.isocalendar()

    # Find the first day of the week.
    start_date = date - timedelta(dow-1)

    # Now, add 6 for the last day of the week (i.e., count up to Sunday)
    end_date = start_date + timedelta(6)

    return (start_date, end_date)


def date_range_of_week(week_number, year=None):

    if year is None:
        year = date.today().isocalendar()[0]
    strip_date = "{}-{}-0".format(year, week_number)
    begin_day_week = datetime.strptime(strip_date, "%Y-%W-%w").date()
    return week_range(begin_day_week)
