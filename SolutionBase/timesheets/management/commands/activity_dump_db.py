import os
import json

from django.core.management.base import BaseCommand
from ...models import TypeOfActivity


class Command(BaseCommand):
    '''
    Command for creating the instances of TypeOfActivity
    from the data in type_of_activity.json
    '''
    def handle(self, *args, **options):

        filepath = os.path.dirname(os.path.abspath(__file__))

        with open(filepath + "/" + "type_of_activity.json", "r") as js_import:
            types_activity = json.load(js_import)

        for type_act in types_activity:

            if not TypeOfActivity.objects.filter(activity_type=type_act["Type of Activity"]).exists():
                TypeOfActivity.objects.create(
                    activity_type=type_act["Type of Activity"],
                    working=type_act["Working"],
                    traveling=type_act["Traveling"],
                    traveling_between_work=type_act["Travel between working hours"],
                    training=type_act["Training"],
                    billable=type_act["Billable"],
                    day_off=type_act["Day Off"],
                    SLA_fee=type_act["SLA Fee"],
                    pause=type_act["Pause"],
                )
