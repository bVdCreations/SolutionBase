from django.urls import path
from .views import TimesheetsBaseView

app_name = 'timesheets'

urlpatterns = [
    path('', TimesheetsBaseView.as_view(),
         name='base'),
]
