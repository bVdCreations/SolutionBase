# from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework.generics import (
                                     CreateAPIView,
                                     UpdateAPIView,
                                     DestroyAPIView,
                                     GenericAPIView
                                     )
from rest_framework import status
from rest_framework.response import Response

from accounts.api.permissions import IsOwner
from ..serializers import TimesheetDaysSerializer, TimesheetWeekSerializer
from ...models import Timesheet
from ...isoweek import date_range_of_week, this_week, this_iso_year

# Create your views here.
# pylint: disable=E1101


class TimesheetWeekListAPIView(GenericAPIView):
    '''
    ApiView that return instances of Timesheets of a week (monday to sunday)
    by defaut it return the instances of this week

    arg url:
        ...?week=<int> return instances of the specified week and of the current year
        ...?week=<int>&year=<int> return instances of te specified week and year

    '''
    serializer_class = TimesheetWeekSerializer
    # permission_classes = [IsOwner, ]

    def get(self, request, *args, **kwargs):
        """get method for this view"""
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset)
        return Response(serializer.data)

    def get_serializer(self, *args, **kwargs):
        """
        Return the serializer instance that should be used for validating and
        deserializing input, and for serializing output.
        """
        serializer_class = self.get_serializer_class()
        kwargs['context'] = self.get_serializer_context()
        kwargs['context'].update({
            'weeknumber': self.get_week_number,
            'year': self.get_year
            })
        return serializer_class(*args, **kwargs)

    def get_queryset(self):
        """
        returns queryset of all the Timesheet instances of a given week
        from the requested user
        """
        user_id = self.request.user.id
        date_range = date_range_of_week(self.get_week_number, year=self.get_year)
        return Timesheet.objects.filter(user=user_id).filter(date__range=date_range)

    @property
    def get_week_number(self)->int:
        """
        return the week number from the request
        or give this it from current week
        """
        week = self.request.GET.get('week')
        if week is None or 0:
            week = this_week()
        return week

    @property
    def get_year(self)->int:
        """
        return the year from the request
        or give this it from current iso year of today
        """
        year = self.request.GET.get('year')
        if year is None or 0:
            year = this_iso_year()
        return year


class TimesheetDayCreateAPIView(CreateAPIView):
    '''
    APIview to create one instance of Timesheet

    arg:
    date : through API json
    user : through request.user

    TODO: new permission classes
    '''
    serializer_class = TimesheetDaysSerializer
    # permission_classes = [IsOwner, ]


class TimesheetDayUpdateAPIView(UpdateAPIView):
    '''
    APIview to update an instance of Timesheets
    '''
    serializer_class = TimesheetDaysSerializer
    permission_classes = [IsOwner, ]
    lookup_field = 'pk'

    def get_queryset(self):
        user_id = self.request.user.id
        return Timesheet.objects.filter(user=user_id)


class TimesheetDayDestroyAPIView(DestroyAPIView):
    '''
    APIview to delete an instance of Timesheets
    condition:
        This instance can only be deleted
        if the Timesheet instance has no instances of activity assigned
    '''
    permission_classes = [IsOwner, ]
    lookup_field = 'pk'

    def get_queryset(self):
        user_id = self.request.user.id
        return Timesheet.objects.filter(user=user_id)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.activities.count() > 0:
            responce_status = status.HTTP_412_PRECONDITION_FAILED
            message = {
                'responce-error':
                'delete is not permitted if instance(s) of activity are assigned to this Timesheet instance'
                }
        else:
            responce_status = status.HTTP_204_NO_CONTENT
            message = {}
            self.perform_destroy(instance)
        return Response(data=message, status=responce_status)
