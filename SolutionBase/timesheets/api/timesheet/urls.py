from django.urls import path

from .views import (TimesheetWeekListAPIView,
                    TimesheetDayCreateAPIView,
                    TimesheetDayUpdateAPIView,
                    TimesheetDayDestroyAPIView)

app_name = 'api_timesheets'

urlpatterns = [
    path('week', TimesheetWeekListAPIView.as_view(),
         name='week'),
    path('day/create', TimesheetDayCreateAPIView.as_view(),
         name='create'),
    path('day/<int:pk>/update', TimesheetDayUpdateAPIView.as_view(),
         name='update'),
    path('day/<int:pk>/delete', TimesheetDayDestroyAPIView.as_view(),
         name='delete'),
]
