from django.urls import path

from .views import (
                    ActivityUpdateAPIView,
                    ActivityDestroyAPIView,
                    TypeOfActivityListAPIView,
                    )
app_name = 'api_activity'

urlpatterns = [
    path('<int:pk>/update', ActivityUpdateAPIView.as_view(),
         name='update'),
    path('<int:pk>/delete', ActivityDestroyAPIView.as_view(),
         name='delete'),
    path('types', TypeOfActivityListAPIView.as_view(),
         name='type_of_activity'),
]
