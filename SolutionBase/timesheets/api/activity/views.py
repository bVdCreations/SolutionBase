from rest_framework.generics import (
                                     UpdateAPIView,
                                     DestroyAPIView,
                                     ListAPIView
                                     )
from rest_framework import status
from rest_framework.response import Response

from ..serializers import ActivitySerializer, TypeOfActivitySerializer
# from accounts.api.permissions import IsOwner

from ...models import Activity, TypeOfActivity

# Create your views here.
# pylint: disable=E1101
# TODO: permissions


class ActivityUpdateAPIView(UpdateAPIView):
    '''
    APIview to update an instance of Activity
    '''
    serializer_class = ActivitySerializer
    # permission_classes = [IsOwner, ]
    lookup_field = 'pk'

    def get_queryset(self):
        user_id = self.request.user.id
        return Activity.objects.filter(timesheet__user=user_id)


class ActivityDestroyAPIView(DestroyAPIView):
    '''
    APIview to delete an instance of Activity
    '''
    # permission_classes = [IsOwner, ]
    lookup_field = 'pk'

    def get_queryset(self):
        user_id = self.request.user.id
        return Activity.objects.filter(timesheet__user=user_id)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        responce_status = status.HTTP_204_NO_CONTENT
        self.perform_destroy(instance)
        return Response(status=responce_status)


class TypeOfActivityListAPIView(ListAPIView):

    queryset = TypeOfActivity.objects.all()
    serializer_class = TypeOfActivitySerializer
