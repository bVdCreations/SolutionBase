from rest_framework import serializers
from rest_framework.generics import get_object_or_404
from ..models import Timesheet, Activity, TypeOfActivity


class ActivitySerializer(serializers.ModelSerializer):
    '''
    serializer used for the model activity
    '''
    until_time = serializers.ReadOnlyField()
    amount_hours = serializers.DecimalField(max_digits=4, decimal_places=2, min_value=0)
    from_time = serializers.TimeField(format="%H:%M")

    class Meta:
        model = Activity
        fields = ('id', 'type_of_activity', 'active_project', 'task',
                  'from_time', 'until_time', 'amount_hours', 'approved')
        read_only_fields = ('id', 'until_time', 'approved')

    def update(self, instance, validated_data):

        instance.type_of_activity = validated_data.get(
            'type_of_activity', instance.type_of_activity)
        instance.active_project = validated_data.get(
            'active_project', instance.active_project)
        instance.task = validated_data.get(
            'task', instance.task)
        instance.from_time = validated_data.get(
            'from_time', instance.from_time)
        instance.amount_hours = validated_data.get(
            'amount_hours', instance.amount_hours)
        instance.save()
        return instance


class TimesheetDaysSerializer(serializers.ModelSerializer):
    '''
    serializer used for the model Timesheet for one day
    '''
    activities = ActivitySerializer(many=True, required=False)

    class Meta:
        model = Timesheet
        fields = ('id', 'user', 'date', 'activities')
        read_only_fields = ('id', 'user', )
        extra_kwargs = {'date': {'required': False}}

    def create(self, validated_data):
        '''
        create a Timesheet instance from the user of the request
        and the given date in validated_data.

        if the validated_data has also activities
        It also creates those instances with the fornkey pointing to this Timesheet instance

        If the Id of an exiting Timesheet is given it will only create activities assigned to the given timesheet
        '''
        if 'activities' in validated_data:
            activities = validated_data.pop('activities')
        else:
            activities = []

        request_obj = self.context.get("request")
        id_timesheet = request_obj.data.get('id', None)

        this_user = self.request_user()
        if id_timesheet is not None:
            timesheet_obj = get_object_or_404(Timesheet.objects.filter(user=this_user), id=id_timesheet)
        else:
            timesheet_obj = Timesheet.objects.create(
                user=this_user, **validated_data)

        for activity in activities:
            Activity.objects.create(timesheet=timesheet_obj, **activity)
        return timesheet_obj

    def request_user(self):
        '''returns the user who made the request or None'''
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
        return user


class TimesheetWeekSerializer(serializers.Serializer):
    '''
    serializer used for the model Timesheet for one week
    '''
    weeknumber = serializers.SerializerMethodField()
    year = serializers.SerializerMethodField()
    days = serializers.SerializerMethodField()

    class Meta:
        fields = ('weeknumber', 'year', 'days')

    def get_weeknumber(self, *args, **kwargs):
        return self.context.get('weeknumber', None)

    def get_year(self, *args, **kwargs):
        return self.context.get('year', None)

    def get_days(self, *args, **kwargs):
        return TimesheetDaysSerializer(*args, many=True, required=False).data


class TypeOfActivityInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = TypeOfActivity
        exclude = ('activity_type', 'id')


class TypeOfActivitySerializer(serializers.ModelSerializer):

    info = serializers.SerializerMethodField()

    class Meta:
        model = TypeOfActivity
        fields = ('activity_type', 'id', 'info')

    def get_info(self, *args, **kwargs):
        return TypeOfActivityInfoSerializer(*args, many=False, required=True).data
        # return 'no info'
