import datetime as dt

from django.db import models
from django.contrib.auth import get_user_model

from projects.models import Project, Task
# Create your models here.


class TypeOfActivity(models.Model):
    '''
    a short list of the possible type of activities
    only admin personal can edit this list
    '''
    activity_type = models.CharField(max_length=100, null=False, blank=False)

    working = models.BooleanField(default=False)
    traveling = models.BooleanField(default=False)
    traveling_between_work = models.BooleanField(default=False)
    training = models.BooleanField(default=False)
    billable = models.BooleanField(default=False)
    day_off = models.BooleanField(default=False)
    SLA_fee = models.BooleanField(default=False)
    pause = models.BooleanField(default=False)

    def __str__(self):
        return self.activity_type


class Timesheet(models.Model):
    '''
    This is used to keep track of the personal performed hours per project
    '''
    user = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)

    # date of the activity
    date = models.DateField()

    def __str__(self):
        return str(self.user) + str(self.date)

    class Meta:
        unique_together = ['user', 'date']


class Comment(models.Model):
    '''this is used to link to big comment in the timesheet'''

    comment = models.CharField(max_length=500, null=False, blank=False)


class Activity(models.Model):
    '''
    this is used to keep al the activity done by a person
    this is linkt to a timesheet by date
    '''
    timesheet = models.ForeignKey(
        Timesheet, on_delete=models.PROTECT, related_name='activities')

    type_of_activity = models.ForeignKey(
        TypeOfActivity, on_delete=models.PROTECT)

    active_project = models.ForeignKey(
        Project, on_delete=models.PROTECT)

    task = models.ForeignKey(
        Task, on_delete=models.PROTECT)

    # start time from the activity
    from_time = models.TimeField()

    # the amount of hours spent on the activity
    amount_hours = models.DecimalField(max_digits=4, decimal_places=2)

    # a comment for the company only
    internal_comment = models.ForeignKey(
        Comment, on_delete=models.SET_NULL, null=True, blank=True, related_name='internal_comment')

    # a comment for the customer
    external_comment = models.ForeignKey(
        Comment, on_delete=models.SET_NULL, null=True, blank=True, related_name='external_comment')

    # after approval there are no edit allowed (expection admin personel)
    approved = models.BooleanField(default=False)

    def until_time(self):
        from_time_date = dt.datetime.combine(self.timesheet.date, self.from_time)
        plus_time = dt.timedelta(seconds=int(self.amount_hours*3600))
        return (from_time_date + plus_time).time().strftime("%H:%M")
