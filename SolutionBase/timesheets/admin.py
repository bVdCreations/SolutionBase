from django.contrib import admin
from .models import Timesheet, Activity, TypeOfActivity
# Register your models here.


class TimesheetAdmin(admin.ModelAdmin):

    # search_fields = []

    list_filter = ['user', 'date', ]

    list_display = [
        'user',
        'date',
         ]

    # list_editable = []


class ActivityAdmin(admin.ModelAdmin):

    # TODO :check all list filters

    # search_fields = []

    list_filter = [
                   'timesheet__user',
                   'type_of_activity',
                   'timesheet__date',
                   'approved'
                   ]

    list_display = [
                    'type_of_activity',
                    'get_user',
                    'get_date',
                    'from_time',
                    'get_until_time',
                    'amount_hours',
                    'approved']

    # list_editable = []

    def get_user(self, obj):
        return obj.timesheet.user
    get_user.short_description = 'User'
    get_user.admin_order_field = 'timesheet__user'

    def get_date(self, obj):
        return obj.timesheet.date
    get_date.short_description = 'date'
    get_date.admin_order_field = 'timesheet__date'

    def get_until_time(self, obj):
        return obj.until_time()
    get_until_time.short_description = 'until time'
    # get_until_time.admin_order_field = 'date'


class TypeOfActivityAdmin(admin.ModelAdmin):

    # search_fields = []

    # list_filter = []

    list_display = ['activity_type', 'working', 'traveling',
                    'traveling_between_work', 'training', 'billable', 'day_off', 'SLA_fee', 'pause']

    list_editable = ['working', 'traveling', 'traveling_between_work',
                     'training', 'billable', 'day_off', 'SLA_fee', 'pause']


admin.site.register(Timesheet, TimesheetAdmin)
admin.site.register(Activity, ActivityAdmin)
admin.site.register(TypeOfActivity, TypeOfActivityAdmin)
