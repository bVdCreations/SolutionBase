// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Timesheet from './Timesheet'
import vClickOutside from 'v-click-outside'
// https://www.npmjs.com/package/v-click-outside

import { store } from './store/store.js';

Vue.config.productionTip = false

Vue.use(vClickOutside)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  components: { App, Timesheet },
  
})
