export function get_base_url() {
    // return the base ur based on the enviroment variables

    var base_url = 'http://127.0.0.1:8000/'

    if (process.env.NODE_ENV !== undefined){
        if (process.env.NODE_ENV === 'development') {
            base_url = 'http://127.0.0.1:8000/'

            
        }else if (process.env.NODE_ENV === 'production') {
            base_url = 'http://solutionbaseproject-dev.eu-west-3.elasticbeanstalk.com/'


        }

    } else 
        
    return base_url
}