// got code from:
// https://codepen.io/Venugopal46/pen/WrxdLY
import { dateTo_Y_M_D } from './date_string.js'
Date.prototype.getWeek = function () {
    var date = new Date(this.getTime()); 
    // getTime = Get the time (milliseconds since January 1, 1970)
    date.setHours(0, 0, 0, 0);
    // dateObj.setHours(hoursValue[, minutesValue[, secondsValue[, msValue]]])

    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    var week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

export function getDateRangeOfWeek(weekNo, y) {
    var d1, numOfdaysPastSinceLastMonday, rangeIsFrom, rangeIsTo;
    d1 = new Date('' + y + '');
    numOfdaysPastSinceLastMonday = d1.getDay() - 1;
    d1.setDate(d1.getDate() - numOfdaysPastSinceLastMonday);
    d1.setDate(d1.getDate() + (7 * (weekNo - d1.getWeek())));
    rangeIsFrom = (d1.getMonth() + 1) + "-" + d1.getDate() + "-" + d1.getFullYear();
    d1.setDate(d1.getDate() + 6);
    rangeIsTo = (d1.getMonth() + 1) + "-" + d1.getDate() + "-" + d1.getFullYear();
    return [rangeIsFrom, rangeIsTo];
};

export function getMondayOfWeek(weekNo, year) {
    var d1, numOfdaysPastSinceLastMonday, MondayOfWeek;
    d1 = new Date('' + year + '');
    d1.setHours(0, 0, 0, 0);
    numOfdaysPastSinceLastMonday = d1.getDay() - 1;
    d1.setDate(d1.getDate() - numOfdaysPastSinceLastMonday);
    d1.setDate(d1.getDate() + (7 * (weekNo - d1.getWeek())));
    MondayOfWeek = dateTo_Y_M_D(d1);

    return MondayOfWeek;
};

export function arrayRangeOfWeek(weeknumber, year){
    var first_day = new Date(getMondayOfWeek(weeknumber, year))
    first_day = first_day.getTime()
    var array_week = []
    const dayms = 24 * 60 * 60 * 1000
    for (var i = 0; i < 7; i++) {
        array_week.push(new Date(first_day + (i * dayms)))
    }
    return array_week;
};
