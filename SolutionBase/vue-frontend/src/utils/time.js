export function to_seconds(timestring){
    // return the amount of second from a string like HH:MM
    var time = timestring.split(':'); // split it at the colons
    return Number(time[0]) * 3600 + Number(time[1]) * 60; 
}

export function to_time(total_seconds){
    // return the time in an string form HH:MM
    total_seconds = Number(total_seconds);
    var h = Math.floor(total_seconds / 3600);
    var m = Math.floor(total_seconds % 3600 / 60);
    if (m<10){
        m = '0' + m.toString()
    } else { m = m.toString()};
    return h.toString() + ':' + m;

}

export function hour_def(first_hour, second_hour){
    // return the hour difference between the first and the second hour
    var sec_def = (to_seconds(second_hour)-to_seconds(first_hour))
    return (sec_def/3600).toFixed(2)
}

export function compare_time(lte_time,gte_time){
    // compare if the first time is earlier or equal then the second
    return to_seconds(lte_time) <= to_seconds(gte_time)
}