export function dateTo_Y_M_D(dateobj) {
    return dateobj.getFullYear() + '-' + (dateobj.getMonth() + 1) + "-" + dateobj.getDate();
};

export function dateTo_d_D_m_Y(dateobj) {
    return day_string[dateobj.getDay()] + ' ' + dateobj.getDate() + ' ' + month_string_short[dateobj.getMonth()] + ' ' + dateobj.getFullYear();
};

export const day_string = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const month_string = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
export const month_string_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
