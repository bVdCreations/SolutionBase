export function array_nonequal(arr1, arr2) {
    // return an array of the elemnts in arr1 that are not in arr2
    var r = [];
    var equal = false
    for (var i = 0; i < arr1.length; i++) {
        for (var j = 0; j < arr2.length; j++) {
            if (arr1[i].getTime() === arr2[j].getTime()) {
                equal = true;
            }
        }
        if (equal === false){
            r.push(arr1[i]);
        }else{
            equal = false;
        }
        
    }
    return r
}

export function timesheet_to_date_array(obj_timesheet){
    // return an array of date obj from all the days in an timesheet obj
    if (obj_timesheet.days !== undefined){
        var result = Object.keys(obj_timesheet.days).map(function (key) {
            var converted_date = new Date(obj_timesheet.days[key].date);
            converted_date.setHours(0, 0, 0, 0)
            return converted_date;
        });
        return result
    }else{return []}
}