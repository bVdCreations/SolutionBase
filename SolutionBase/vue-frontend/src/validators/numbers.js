export function is_float(timestring) {
    // check is the string is of this format 15  15.1 25.35 1.23  1.2 or ...
    var pattr1 = /(^\d{1,2})(?!\d|\w)([.](\d{1,2})?)?(?!\d|\W|\w)/
    return pattr1.test(timestring)

}
