export function valid_input_time(timestring) {
    if (typeof timestring === 'string' && timestring.includes(':')){
        var timearr = timestring.split(':')
        if(timearr.length === 2){
            var ishours = false
            var hour = timearr[0]
            if (hour.length >= 1 && hour.length <= 2 && Number.isInteger(Number(hour))){
                if (Number(hour) >= 0 && Number(hour) < 24) {
                    ishours = true
                }
            };
            var minutes = timearr[1]
            var isminutes = false
            if (minutes.length === 2 && Number.isInteger(Number(minutes))) {
                if (Number(minutes) >= 0 && Number(minutes) < 60) {
                    isminutes = true
                }
            };
            if (ishours && isminutes){
                return true
            }else{
                // console.log('ishours', ishours)
                // console.log('isminutes', isminutes)
                return false
            }
            
        }
        else {

            // console.log('only use 1 : ', timestring)
            return false
            }
    }else{
        // console.log('no valid string', timestring)
        return false
    }    
}



