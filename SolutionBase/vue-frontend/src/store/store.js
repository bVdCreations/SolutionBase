import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import Cookies from 'js-cookie';

import {get_base_url} from '../utils/base_url.js'

axios.defaults.baseURL = get_base_url();
axios.defaults.headers.common['Authorization'] =
    'Bearer ' + localStorage.getItem('jwt');

const csrftoken = Cookies.get('csrftoken'); // Using JS Cookies library
axios.defaults.headers.post['X-CSRFToken'] = csrftoken
axios.defaults.headers.put['X-CSRFToken'] = csrftoken
axios.defaults.headers.patch['X-CSRFToken'] = csrftoken
axios.defaults.headers.delete['X-CSRFToken'] = csrftoken

// modules
import Timesheet from './modules/timesheet.js'
import JWT from './modules/jwt.js'
import Activity from './modules/activity.js'
import UrlPath from './modules/url_path.js'
import Projects from './modules/projects.js'

Vue.use(Vuex);

export const store = new Vuex.Store({
    state:{
        
    },
    getters: {

    },
    actions: {

    },
    mutations: {

    },
    modules: {
        Timesheet,
        Activity,
        JWT,
        UrlPath,
        Projects,
    }

});

