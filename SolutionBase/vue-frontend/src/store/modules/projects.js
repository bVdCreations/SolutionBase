import axios from 'axios';

const state = {
    projects: []
};

const getters = {
    get_projects: function (state) {
        return state.projects;
    }
};

const actions = {
    LOAD_PROJECTS: function ({ dispatch }) {
        if (state.projects.length == 0) {
            dispatch('RELOAD_PROJECTS');
        }
    },
    RELOAD_PROJECTS: function ({ commit, rootState }) {
        
        axios
            .get(rootState.UrlPath.project.all + '?format=json')
            .then( // handle success
                (response) => {
                    commit('SET_PROJECTS', { list: response.data })
                },
        )
            .catch( // handle error
                (error) => {
                    console.log(error)
                }
            )
            .then( // always executed
            )
    },
    
};

const mutations = {
    SET_PROJECTS: (state, { list }) => {
        state.projects = list
    },
};

export default {
    state: state,
    getters: getters,
    actions: actions,
    mutations: mutations
}