// https://hackernoon.com/jwt-authentication-in-vue-js-and-django-rest-framework-part-2-788f0ad53ad5
import axios from 'axios';
import jwt_decode from 'jwt-decode';

const state = {
    jwt: localStorage.getItem('jwt'),
};

const getters = {
    
};

const actions = {
    obtainToken({ rootState },username,password) {
        const payload = {
            username: username,
            password: password
        };
        console.log(state.endpoints);
        axios
            .post(rootState.UrlPath.obtainJWT, payload)
            .then((response) => {
                this.commit('updateToken', response.data.token);
            })
            .catch((error) => {
                console.log(error);
            })
    },

    refreshToken({ rootState }) {
        const payload = {
            token: state.jwt
        }
        axios
            .post(rootState.UrlPath.refreshJWT, payload)
            .then((response) => {
                this.commit('updateToken', response.data.token)
            })
            .catch((error) => {
                console.log(error)
            })
    },

    inspectToken() {
        const token = state.jwt;
        if (token) {
            const decoded = jwt_decode(token);
            const exp = decoded.exp
            const orig_iat = decode.orig_iat
            if (exp - (Date.now() / 1000) < 1800 && (Date.now() / 1000) - orig_iat < 628200) {
                this.dispatch('refreshToken')
            } else if (exp - (Date.now() / 1000) < 1800) {
                // DO NOTHING, DO NOT REFRESH          
            } else {
                // PROMPT USER TO RE-LOGIN, THIS ELSE CLAUSE COVERS THE CONDITION WHERE A TOKEN IS EXPIRED AS WELL
            }
        }
    }
};

const mutations = {
    updateToken(state, newToken) {
        localStorage.setItem('jwt', newToken);
        state.jwt = newToken;
    },
    removeToken(state) {
        localStorage.removeItem('jwt');
        state.jwt = null;
    }
};

export default {
    state: state,
    getters: getters,
    actions: actions,
    mutations: mutations
}