import axios from 'axios';

const state = {
    timesheets : {
        timesheetweek :[],
        apierror: '',
        loading : false,

    },
    loading : false
};

const getters = {
    get_timesheet_data: state => {
        return state.timesheets.timesheetweek;
    },
    loading: state => {
        return state.loading
    }
};

const actions =  {
    LOAD_THIS_WEEK: function ({ dispatch, rootState }) {
        var url = rootState.UrlPath.timesheet.week + '?format=json'
        dispatch('LOAD_WEEK_URL',url)
    },
    LOAD_WEEK: function ({ dispatch, rootState }, week , year) {
        var url = rootState.UrlPath.timesheet.week + '?format=json' + '&week=' + week
        if (year != null){
            url = url + '&year=' + year
        }
        dispatch('LOAD_WEEK_URL', url)
    },
    LOAD_CURRENT_WEEK: function ({ dispatch }) {
        var week = state.timesheets.timesheetweek.weeknumber
        var year = state.timesheets.timesheetweek.year
        dispatch('LOAD_WEEK', week, year)
    },
    LOAD_WEEK_URL: function ({ commit },url) {
        commit('SET_LOADING_TIMESHEET', true),
        commit('SET_LOADING', true),
        axios
            .get(url)
            .then( // handle success
                (response) => {
                    commit('SET_TIMESHEET', { list: response.data })
                },
                commit('SET_ERROR_TIMESHEET', '')
            )
            .catch( // handle error
                (error) => {
                    console.log(error),
                    commit('SET_ERROR_TIMESHEET', error)
                }
            )
            .then( // always executed
                commit('SET_LOADING_TIMESHEET', false),
                commit('SET_LOADING', false)
            )
    },
    CREATE_DAY: function({dispatch, commit, rootState},data) {
        commit('SET_LOADING_TIMESHEET', true),
        commit('SET_LOADING', true),        
        axios
            .post(rootState.UrlPath.timesheet.create_day, data)
            .then( // handle success
                (response) => {                 
                    dispatch('LOAD_CURRENT_WEEK'),
                    commit('SET_ERROR_TIMESHEET', '')
                },
            )
            .catch( // handle error
                (error) => {
                    console.log(error),
                    commit('SET_ERROR_TIMESHEET', error)
                }
            )
            .then( // always executed
                commit('SET_LOADING_TIMESHEET', false),
                commit('SET_LOADING', false)
            )
    },
    CREATE_NEXT_DAY:function({dispatch}){
        var listdates = state.timesheets.timesheetweek.days;
        var last_day = new Date(listdates[listdates.length -1].date);
        var next_day = new Date(last_day.getTime() + (24 * 60 * 60 * 1000));
        var next_day_string = next_day.getFullYear() + "-" + (next_day.getMonth() + 1) + "-" + next_day.getDate();

        dispatch('CREATE_DAY', { "date": next_day_string})
    },

    DELETE_DAY: function ({ dispatch, commit, rootState }, id) {

            axios
                .delete(rootState.UrlPath.timesheet.delete_day + id + '/delete')
                .then( // handle success
                    (response) => {
                        dispatch('LOAD_CURRENT_WEEK')
                    },
            )
                .catch( // handle error
                    (error) => {
                        console.log(error),
                            commit('SET_ERROR_TIMESHEET', error)
                    }
                )
                .then( // always executed
                )
    }
};

const mutations = {
    SET_LOADING_TIMESHEET: (state, bool) => {
        state.timesheets.loading = bool
    },
    SET_LOADING: (state, bool) => {
        state.loading = bool
    },
    SET_TIMESHEET: (state, { list }) => {
        state.timesheets.timesheetweek = list
    },
    SET_ERROR_TIMESHEET: (state, error_string) => {
        state.timesheets.error = error_string
    },

};

export default {
    state:state,
    getters:getters,
    actions:actions,
    mutations:mutations
}