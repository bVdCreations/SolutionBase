import axios from 'axios';

const state = {
    activity_types : []
};

const getters = {
    get_activity_types: function(state){
        return state.activity_types;
    } 
};

const actions = {
    LOAD_ACTIVITY_TYPES: function ({ dispatch }) {
        if (state.activity_types.length == 0) {
            dispatch('RELOAD_ACTIVITY_TYPES');
        }
    },  
    RELOAD_ACTIVITY_TYPES: function ({ commit, rootState }) {
        axios
            .get(rootState.UrlPath.activity.types + '?format=json')
            .then( // handle success
                (response) => {
                    commit('SET_ACTIVITY_TYPES', { list: response.data })
                },
            )
            .catch( // handle error
                (error) => {
                    console.log(error)
                }
            )
            .then( // always executed
            )
    },
    CREATE_ACTIVITY: function ({ dispatch, commit, rootState }, data_act){

        axios
            .post(rootState.UrlPath.activity.create_activity, data_act)
            .then( // handle success
                (response) => {
                    dispatch('LOAD_CURRENT_WEEK')
                }
                
            )
            .catch( // handle error
                (error) => {
                    console.log(error),
                    commit('SET_ERROR_TIMESHEET', error)
                }
            )
            .then( // always executed
            )
    },
    UPDATE_ACTIVITY: function({ dispatch, commit, rootState }, data_obj){
        console.log(data_obj)
        var url_update = rootState.UrlPath.activity.update + data_obj.act_id.toString() + '/update';
        axios
            .put(url_update, data_obj.activity_data)
            .then( // handle success
                (response) => {
                    dispatch('LOAD_CURRENT_WEEK')
                }

            )
            .catch( // handle error
                (error) => {
                    console.log(error),
                        commit('SET_ERROR_TIMESHEET', error)
                }
            )
            .then( // always executed
            )
    },
    

    DELETE_ACTIVITY: function ({ commit, dispatch, rootState }, id) {

        axios
            .delete(rootState.UrlPath.activity.delete + id + '/delete')
            .then( // handle success
                (response) => {
                
                dispatch('LOAD_CURRENT_WEEK')
                }

            )
            .catch( // handle error
                (error) => {
                    console.log(error)
                }
            )
            .then( // always executed
            )
    }
};

const mutations = {
    SET_ACTIVITY_TYPES: (state, { list }) => {
        state.activity_types = list
    },
};

export default {
    state: state,
    getters: getters,
    actions: actions,
    mutations: mutations
}