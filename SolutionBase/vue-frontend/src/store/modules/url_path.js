const state = {   
    JWT:{
        obtainJWT: '/auth/obtain_token',
        refreshJWT: '/auth/refresh_token'},
    timesheet:{
        week:'/api/timesheet/week',
        create_day:'/api/timesheet/day/create',
        update_day: '/api/timesheet/day/',
        delete_day: '/api/timesheet/day/',
    },
    activity:{
        types: '/api/activity/types',
        create_activity: '/api/timesheet/day/create',
        update: '/api/activity/',
        delete: '/api/activity/',
    },
    project:{
        all: '/api/project/all',
    },
};
const getters = {
    get_endpoints: state => {
        return state.endpoints;
    },
};

export default {
    state: state,
    getters:getters,
}