from enum import Enum


class ChoiceCharEnum(Enum):
    '''used for the easy use choices in the models for charfields'''
    @classmethod
    def choices(cls):
        return tuple((x.name, x.value) for x in cls)


class ChoiceIntEnum(Enum):
    '''used for the easy use choices in the models for Intergerfields'''
    @classmethod
    def choices(cls):
        return tuple((x.value, x.name) for x in cls)

    def __str__(self):
        return self.name

    def __int__(self):
        return self.value


''' EXAMPLE

class Intensisty(ChoiceIntEnum):

    LOW = 1
    NORMAL = 2
    HIGH = 3

intensity = models.PositiveSmallIntegerField(
        default= int(Intensisty.NORMAL), choices=Intensisty.choices())



class MussleGroup (ChoiceCharEnum):

    LEGS = 'legs'
    CHEST = 'chest'

mussle_group = models.CharField(
    max_length=10, choices=MussleGroup.choices(), blank=False, null=True)

'''
