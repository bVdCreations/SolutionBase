from django.urls import path

from .views import (
    AllProjectsListAPIView
)
app_name = 'api_project'

urlpatterns = [
    path('all', AllProjectsListAPIView.as_view(),
         name='allprojects')
]
