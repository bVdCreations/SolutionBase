from rest_framework.generics import (
                                    ListAPIView
                                    )

from ..models import Project
from .serializers import AllProjectsSerializer


class AllProjectsListAPIView(ListAPIView):
    '''this a temperally view'''

    queryset = Project.objects.all()
    serializer_class = AllProjectsSerializer
