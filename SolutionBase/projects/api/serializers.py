from rest_framework import serializers
from ..models import Project


class AllProjectsSerializer(serializers.ModelSerializer):
    '''
    serializer used for the model activity
    '''

    class Meta:
        model = Project
        fields = '__all__'
