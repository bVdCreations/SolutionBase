from django.db import models
from SolutionBase.utils import ChoiceCharEnum


# Create your models here.
class Project(models.Model):
    '''
    a short list of the possible type of activities
    only admin personal can edit this list
    '''
    name_project = models.CharField(max_length=100, null=False, blank=False)

    discription = models.CharField(max_length=300, null=False, blank=False)

    project_owner = models.CharField(max_length=100, null=False, blank=False)

    class StatusChoices (ChoiceCharEnum):

        ACT = 'Active'
        HOLD = 'On hold'
        CANC = 'Canceld'
        DONE = 'Finished'

    status = models.CharField(
        max_length=4, choices=StatusChoices.choices(), blank=False, null=True)

    def __str__(self):
        return self.name_project


class Task(models.Model):
    '''
    project can be split up in mutiple task
    '''
    task_name = models.CharField(max_length=100, null=False, blank=False)

    completed = models.BooleanField(default=False)

    def __str__(self):
        return self.task_name
