from django.contrib import admin
from .models import Project, Task
# Register your models here.


class ProjectAdmin(admin.ModelAdmin):

    # search_fields = []

    list_filter = ['project_owner', 'status']

    list_display = ['name_project', 'project_owner', 'status', 'discription']

    # list_editable = []


class TaskAdmin(admin.ModelAdmin):

    # search_fields = []

    list_filter = ['completed']

    list_display = ['task_name', 'completed']

    # list_editable = []


admin.site.register(Project, ProjectAdmin)
admin.site.register(Task, TaskAdmin)
