from django.contrib import admin
from .models import User
# Register your models here.


class UserAdmin(admin.ModelAdmin):

    # search for users based on username
    search_fields = ['first_name', 'last_name']

    # add filter to filter on user type
    list_filter = ['department', 'location']

    # add display off usertype
    list_display = ['username', 'first_name',
                    'last_name', 'email', 'department', 'location', 'birth_date']

    # make items in display list editable
    list_editable = []


admin.site.register(User, UserAdmin)
