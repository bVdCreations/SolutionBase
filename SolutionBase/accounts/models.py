from django.db import models
from django.contrib.auth.models import AbstractUser

from SolutionBase.utils import ChoiceCharEnum
# Create your models here.


# Extending User Model Using a Custom Model Extending AbstractUser
# https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#abstractuser
class User (AbstractUser):
    """
    Extended User Model
    """
    birth_date = models.DateField(null=True, blank=True)

    class Department (ChoiceCharEnum):
        '''Departement choices'''
        AUT0 = 'Automation team'
        MIT = 'Manufactering IT'
        ADMIN = 'administration'
        SALES = 'sales'
        CEO = 'The Boss'
        INT = 'intern/student'
        THIRD = 'Third party'

    department = models.CharField(
        max_length=5, choices=Department.choices(), blank=False, null=True)

    class Location (ChoiceCharEnum):
        '''Location choices'''
        BE = 'Belguim'
        ES = 'Spain'

    location = models.CharField(
        max_length=3, choices=Location.choices(), blank=False, null=True)

    def __str__(self):
        return self.username
