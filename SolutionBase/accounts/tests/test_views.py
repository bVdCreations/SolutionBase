from django.test import TestCase

from django.urls import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth import logout
User = get_user_model()


class TestLogin(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        self.user = User.objects.create_user(**self.credentials)
        # send login data
        self.path = reverse('accounts:login')
        self.response = self.client.post(self.path, self.credentials, follow=True)

    def test_login(self):

        # should be logged in now
        self.assertEqual(self.response.status_code, 200)
        self.assertTrue(self.response.context['user'].is_authenticated)
        self.assertRedirects(self.response, reverse('accounts:logged_in'))

    # def test_logout(self):
    #
    #     self.path = reverse('accounts:logout')
    #     self.response = self.client.logout()
    #     self.assertEqual(self.response.status_code, 200)
    #     self.assertRedirects(self.response, reverse('accounts:logout'))


class TestAnonymous(TestCase):

    def test_login_required_logged_in(self):

        # url that needs a login required
        login_required_url = reverse('accounts:logged_in')

        # url to be direct if login required is not true
        redirected_url = reverse('accounts:login')

        # get the response of the getr method
        response = self.client.get(login_required_url, follow=True)

        self.assertRedirects(response, redirected_url+'?next='+login_required_url)
