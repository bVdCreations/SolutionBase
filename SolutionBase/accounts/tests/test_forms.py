from django.test import TestCase
from ..forms import UserCreateForm


class MyTests(TestCase):
    def test_forms(self):
        form_data = {"username": 'testforms', 'id_first_name': 'jaak', 'last_name': 'test case', 'email': 'test@test.com', 'password1': 'Thepassw01-', 'password2': 'Thepassw01-', 'user_type': '1'}
        form = UserCreateForm(data=form_data)
        self.assertTrue(form.is_valid(), u'form = {}'.format(form.errors))

    # def test_blank_data(self):
    #     form = UserCreateForm({})
    #     # self.assertFalse(form.is_valid())
    #     self.assertEqual(form.errors, {
    #         'username': ['required'],
    #         # 'first_name': ['required'],
    #         # 'last_name': ['required'],
    #         'email': ['required'],
    #         'password1': ['required'],
    #         'password2': ['required'],
    #         'user_type': ['required'],
    #     }, u'test_blank_data form.errors = {}'.format(form.errors))
