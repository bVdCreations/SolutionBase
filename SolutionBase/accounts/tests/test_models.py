from django.test import TestCase
from django.test import Client

from django.contrib.auth.models import Permission, Group
from django.contrib.auth import get_user_model
User = get_user_model()


class UserTest (TestCase):

    @classmethod
    def setUpTestData(cls):
        # print("setUpTestData: Run once to set up non-modified data for all class methods.")
        pass

    def setUp(self):
        # create permissions group
        group_name = "My Test Group"
        self.group = Group(name=group_name)
        self.group.save()
        self.c = Client()
        self.user = User.objects.create_user(username="test", email="test@test.com", password="test")

    def tearDown(self):
        # Clean up run after every test method.
        self.user_test.delete()


class UserTest_client(TestCase):
    def setUp(self):
        self.user_client = User.objects.create(username='run_test_client', user_type=1, email="test@test.com", password="test")

    def test_created_user(self):
        # test if user is created
        self.assertTrue(self.user_client in User.objects.all())

    def test_is_not_default(self):
        # test if the usertype is not set on default
        self.assertFalse(self.user_client.user_type == 0)

    def test_is_client(self):
        # test if the user is a client

        self.assertTrue(self.user_client.user_type == 1)
        self.assertTrue(self.user_client.is_client())

    def test_is_not_coach(self):
        # test if the user is not a coach

        self.assertFalse(self.user_client.user_type == 2)
        self.assertFalse(self.user_client.is_coach())

    def tearDown(self):
        # Clean up run after every test method.
        self.user_client.delete()


class UserTest_coach(TestCase):
    def setUp(self):
        self.user_coach = User.objects.create(username='run_test_coach', user_type=2, email="test@test.com", password="test")

    def test_created_user(self):
        # test if user is created
        self.assertTrue(self.user_coach in User.objects.all())

    def test_is_not_default(self):
        # test if the usertype is not set on default
        self.assertFalse(self.user_coach.user_type == 0)

    def test_is_coach(self):
        # test if the user is a coach

        self.assertTrue(self.user_coach.user_type == 2)
        self.assertTrue(self.user_coach.is_coach())

    def test_is_not_client(self):
        # test if the user is not a client

        self.assertFalse(self.user_coach.user_type == 1)
        self.assertFalse(self.user_coach.is_client())

    def tearDown(self):
        # Clean up run after every test method.
        self.user_coach.delete()
