from django.core.management.base import BaseCommand
from accounts.models import User

# This is based on the tutorial on:
# https://realpython.com/deploying-a-django-app-and-postgresql-to-aws-elastic-beanstalk/
# configuring a database > create admin user


class Command(BaseCommand):
    # command for creating auto superuser
    def handle(self, *args, **options):
        if not User.objects.filter(username="base_admin").exists():
            User.objects.create_superuser("base_admin", email="test@test.com", password="$0lut10nBASE")
