# from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from . import forms

# Create your views here.


class SignUp (CreateView):
    # use the standard form for creating user
    form_class = forms.UserCreateForm

    # after complete go to
    success_url = reverse_lazy('accounts:login')

    # html name for the form
    template_name = 'registration/signup.html'


class LoggedIn(LoginRequiredMixin, TemplateView):
    template_name = 'accounts/logged_in.html'


class LogOut(TemplateView):
    template_name = 'accounts/logout.html'
