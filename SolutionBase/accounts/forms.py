from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm


# sign up form
class UserCreateForm (UserCreationForm):

    email = forms.CharField(max_length=254, required=True, widget=forms.EmailInput())
    birth_date = forms.DateField(
        input_formats=['%Y-%m-%d', '%Y/%m/%d/', '%y/%m/%d/', '%y-%m-%d'], label='birth date: y-m-d')

    class Meta():
        # fields for sign up
        fields = ('username',
                  'first_name',
                  'last_name',
                  'email',
                  'password1',
                  'password2',
                  'department',
                  'location',
                  )
        # get the current user model
        model = get_user_model()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # rename fields for sign up
        self.fields['username'].label = 'Display Name'
        self.fields['email'].label = 'Email Adress'
